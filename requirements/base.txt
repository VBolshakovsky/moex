# local package(setup.py)
#-e .\\moex_gate
yapf
python-dotenv
pika
redis
JsonFormatter
opensearch-py
opensearch-logger
pandas
fastapi
uvicorn==0.16.0
celery==5.3.0
flower==1.2.0

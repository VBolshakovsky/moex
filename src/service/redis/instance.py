"""
Description: Create instance Redis
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 11.05.2023
Links:
    https://pythonru.com/biblioteki/redis-python
    https://github.com/redis/redis
    Команды
    https://redis-py.readthedocs.io/en/stable/commands.html
    RedisJSON
    https://developer.redis.com/howtos/redisjson/using-python/
    https://pypi.org/project/rejson/
Comment:
"""

from client import RedisClient
from src.config import Settings as settings


def redis() -> RedisClient:
    """
    Функция, которая создает и возвращает экземпляр класса Redis
    """
    return RedisClient(host=settings.REDIS_HOST,
                       port=settings.REDIS_PORT,
                       db=settings.REDIS_DB,
                       password=settings.REDIS_PASSWORD)

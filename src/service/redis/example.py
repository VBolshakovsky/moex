"""
Description: Examples of using for Redis
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 11.05.2023
Links:
    https://pythonru.com/biblioteki/redis-python
    https://github.com/redis/redis
    Команды
    https://redis-py.readthedocs.io/en/stable/commands.html
    RedisJSON
    https://developer.redis.com/howtos/redisjson/using-python/
    https://pypi.org/project/rejson/
Comment:
"""
from client import RedisClient

r = RedisClient(password="qwerty")
#Методы работы с string
r.set("user:string", "Jhon")
print(r.get("user:string"))

#Методы работы с hash
r.set_m_hash('user:hash', {'name': 'John', 'age': 30})
r.set_hash("user:hash", "gender", "man")
r.set_hash("user:hash", "age", 35)
r.del_hash("user:hash", "gender")
print(r.get_hash_exists("user:hash", "age"))
print(r.get_hash("user:hash", 'name'))
print(r.get_hash_all("user:hash"))

#Методы работы с list
r.set_list('user:list', 'value1', 'value2', 'value3')
r.set_list_index('user:list', 0, 'value4')
r.del_list('user:list', 0, 'value4')
print(r.get_list('user:list'))
print(r.get_list_index('user:list', 0))
print(r.search_list('user:list', 'value3'))

#Методы работы с json
r.set_json('user:json', {'name': 'John', 'age': 30})
print(r.get_json('user:json'))

#Методы общие
print(r.type('user:list'))
r.expire('user:list', 60)
r.set("user:1", "Jhon")
r.set("user:2", "Jhon")
r.delete("user:1", "user:2")
print(r.get_all_keys())
print(r.get_all_keys(include_type=True, include_value=True))
r.flush()
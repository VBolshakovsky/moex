"""
Description: Working with logs
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 03.05.2023
Links:
    https://docs.python.org/3/library/logging.html
    https://digitology.tech/docs/python_3/howto/logging-cookbook.html
    https://stackabuse.com/how-to-print-colored-text-in-python/
    https://docs-python.ru/standart-library/paket-logging-python/prostoe-ispolzovanie-modulja-logging/
"""

import os
import logging
from logging.handlers import RotatingFileHandler

from opensearch_logger import OpenSearchHandler
from jsonformatter import JsonFormatter
from src.config import Settings as settings

# Строка с указанием формата JSON для jsonformatter
STRING_FORMAT = {
    "Name": "name",
    "Levelno": "levelno",
    "Levelname": "levelname",
    "Pathname": "pathname",
    "Filename": "filename",
    "Module": "module",
    "Lineno": "lineno",
    "FuncName": "funcName",
    "Created": "created",
    "Asctime": "asctime",
    "Msecs": "msecs",
    "RelativeCreated": "relativeCreated",
    "Thread": "thread",
    "ThreadName": "threadName",
    "Process": "process",
    "Message": "message"
}


class FormatterConsole(logging.Formatter):
    """
    Класс для обогащения консольного вывода для добавление цвета
    """

    grey = "\033[2;30m"
    red = "\033[2;31m"
    green = "\033[2;32m"
    yellow = "\033[2;33m"
    blue = "\033[2;34m"
    burgundy = "\033[2;35m"
    cyan = "\033[2;36m"
    white = "\033[2;37m"
    closing = "\033[0;0m"

    body = settings.LOG_FMT

    FORMATS = {
        logging.DEBUG: grey + body + closing,
        logging.INFO: green + body + closing,
        logging.WARNING: yellow + body + closing,
        logging.ERROR: red + body + closing,
        logging.CRITICAL: burgundy + body + closing
    }

    def format(self, record) -> str:
        """
        Record conversion depending on the level
        """

        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

    @classmethod
    def colors_16(cls) -> str:
        """
        Метод выводит в консоль окрашенные коды для 16 схемы
        """

        colors_16_str = ""

        for i in range(30, 38):
            colors_16_str = colors_16_str + (
                f"\033[2;{i}m \\033[2;{i}m \033[0;0m")

        print(colors_16_str)

    @classmethod
    def colors_256(cls) -> str:
        """
        Метод выводит в консоль окрашенные коды для 256 схемы
        """

        colors_256_str = ""

        for i in range(256):
            num1 = str(i)
            num2 = str(i).ljust(3, ' ')
            if i % 6 == 0:
                colors_256_str = colors_256_str + (
                    f"\033[38;5;{num1}m \\033[38;5;{num2}m \033[0;0m\n")
            else:
                colors_256_str = colors_256_str + (
                    f"\033[38;5;{num1}m \\033[38;5;{num2}m \033[0;0m")

        print(colors_256_str)


def init_logger(level_info: int = logging.DEBUG,
                level_error: int = logging.ERROR,
                console: bool = True,
                file_error: bool = True,
                file_full: bool = True,
                open_search: bool = True) -> logging.Logger:
    """ 
    ### Функция создания логгера
    """

    logger = logging.getLogger(settings.APP_NAME)

    if not logger.handlers:
        # Задаем уровень срабатывания для логгера
        logger.setLevel(level_info)

        # Создаем форматтеры для обработчиков
        formatter_file = logging.Formatter(settings.LOG_FMT,
                                           datefmt=settings.LOG_DATEFMT)
        formatter_consol = FormatterConsole()
        formatter_json = JsonFormatter(STRING_FORMAT, ensure_ascii=False)

        # Console
        if console:
            # Создаем обработчик для логгера
            handler_console = logging.StreamHandler()
            # Задаем формат для обработчику
            handler_console.setFormatter(formatter_consol)
            # Задаем уровень срабатывания для обработчика
            handler_console.setLevel(level_info)
            # Добавляем обработчик в логгер
            logger.addHandler(handler_console)

        # FileError
        if file_error:
            # Указываем директорию для обработчика
            log_file_error = os.path.join(
                os.getcwd(), settings.LOG_PATH,
                f'{settings.APP_NAME.lower()}_error.json')

            # Создаем обработчик для логгера
            handler_file_error = RotatingFileHandler(
                log_file_error,
                mode='a',
                maxBytes=10485760,  #10 МБ
                backupCount=4,
                encoding='utf-8',
                delay=0)

            # Задаем формат для обработчика
            handler_file_error.setFormatter(formatter_json)

            # Задаем уровень срабатывания для обработчика
            handler_file_error.setLevel(level_error)

            # Добавляем обработчик в логгер
            logger.addHandler(handler_file_error)

        # FileFull
        if file_full:
            # Указываем директорию для обработчика
            log_file_full = os.path.join(
                os.getcwd(), settings.LOG_PATH,
                f'{settings.APP_NAME.lower()}_full.log')

            # Создаем обработчик для логгера
            handler_file_full = RotatingFileHandler(
                log_file_full,
                mode='a',
                maxBytes=10485760,  #10 МБ
                backupCount=4,
                encoding='utf-8',
                delay=0)

            # Задаем формат для обработчика
            handler_file_full.setFormatter(formatter_file)

            # Задаем уровень срабатывания для обработчика
            handler_file_full.setLevel(level_info)

            # Добавляем обработчик в логгер
            logger.addHandler(handler_file_full)

        # OpenSearch
        if open_search:
            # Проверим протокол хоста OpenSearch
            url_search = settings.OPEN_SEARCH_HOST
            if url_search.startswith("http://"):
                use_ssl = False
            else:
                use_ssl = True

            # Создаем обработчик для логгера
            handler_open_search = OpenSearchHandler(
                index_name=settings.OPEN_SEARCH_INDEX,
                hosts=[settings.OPEN_SEARCH_HOST],
                http_auth=(settings.OPEN_SEARCH_USER,
                           settings.OPEN_SEARCH_PASSWORD),
                http_compress=True,
                index_name_sep="_",
                index_rotate="MONTHLY",
                index_date_format="%Y.%m.%d",
                use_ssl=use_ssl,
                verify_certs=False,
                ssl_assert_hostname=False,
                ssl_show_warn=False)

            # Задаем уровень срабатывания для обработчика
            handler_open_search.setLevel(level_info)

            # Добавляем обработчик в логгер
            logger.addHandler(handler_open_search)
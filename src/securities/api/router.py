from typing import Optional
from fastapi import APIRouter
from src.securities.security import Security

from src.moex.api.schemas import ModelEngines
from src.moex.api.schemas import ModelMarkets
from src.moex.api.schemas import ModelSecuritytypes
from src.moex.api.schemas import ModelLang
from src.moex.moex import Moex

router_securities = APIRouter()


@router_securities.get("/")
def securities(
    q: Optional[str] = None,
    engine: Optional[ModelEngines] = None,
    market: Optional[ModelMarkets] = None,
    is_trading: Optional[bool] = None,
    group_by_filter: Optional[ModelSecuritytypes] = None,
    lang: Optional[ModelLang] = None,
):
    """
    ### Метод для получения списка бумаг торгуемых на московской бирже
        https://iss.moex.com/iss/reference/5
        Выдается частями(может быть очень много записей), подумать о ассинхронности
        
    Args:
    - q                 : поиск инструментов по части Кода, Названию, ISIN,
                        Идентификатору Эмитента, Номеру гос.регистрации.
                        Слова длиной менее трёх букв игнорируются. 
                        Если параметром передано два слова через пробел. 
                        То каждое должно быть длиной не менее трёх букв.
    - engine            : поиск инструментов по торговой системе
    - market            : поиск инструментов по рынку
    - is_trading        : торгуются?
    - group_by_filter   : фильтровать по типам group или type. Зависит от значения фильтра group_by
        - group / https://iss.moex.com/iss/securitygroups.json
    Example:
    https://iss.moex.com/iss/securities.json
    """
    return Moex.get_securities(
        q=q,
        engine=engine.value if engine is not None else engine,
        market=market.value if market is not None else market,
        is_trading=is_trading,
        group_by="type",
        group_by_filter=group_by_filter.value
        if group_by_filter is not None else group_by_filter,
        lang=lang.value if lang is not None else lang)


@router_securities.get("/{security}")
def security(security: str, title_ru: bool = False):
    """
    ### Метод для получения общей информации о ценной бумаге
    """
    security = Security(security)

    properties = security.get_info()
    rez = {}
    if title_ru:
        for key in properties.keys():
            rez[security.property_name[key]] = properties[key]
        return rez
    return properties


@router_securities.get("/{security}/boards")
def security_boards(security: str):
    """
    ### Метод для получения режимов торгов ценной бумаги
    """
    security = Security(security)
    return security.get_boards()


@router_securities.get("/{security}/board_primary")
def security_board_primary(security: str):
    """
    ### Метод для получения основного режима торгов ценной бумаги
    """
    security = Security(security)
    return security.get_board_primary()


@router_securities.get("/{security}/coupons")
def security_coupons(security: str):
    """
    ### Метод для получения графика выплаты купонов(только для облигаций)
    """
    security = Security(security)
    return security.get_bond_coupons()


@router_securities.get("/{security}/amortizations")
def security_amortizations(security: str):
    """
    ### Метод для получения суммированной доходности по купонам(только для облигаций)
    """
    security = Security(security)
    return security.get_bond_amortizations()


@router_securities.get("/{security}/dividends")
def security_dividends(security: str):
    """
    ### Метод для получения дивидентов(только для акций)
    """
    security = Security(security)
    return security.get_share_dividends()


@router_securities.get("/{security}/history_tradingy")
def security_history_tradingy(security: str,
                              date_from: str,
                              date_till: str = None):
    """
    ### Метод для получения истории торгов по группе режима торгов
    
    Args:
    - date_from     : дата, начиная с которой необходимо начать выводить данные
    - date_till     : дата, до которой выводить данные
    """
    security = Security(security)
    return security.get_history_trading_from_board(date_from=date_from,
                                                   date_till=date_till)

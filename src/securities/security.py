"""
Description: Working with securities in Moscow exchange)
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 20.06.2023
Links:
    https://www.moex.com/
Comment:
"""

from src.moex.moex import Moex
from typing import Optional


class SecurityError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class Security:
    """
    ### Класс ценная бумага
    
    Args:
    - secid : код ценной бумаги на мос бирже
    """

    def __init__(self, secid: str):
        self.secid = secid
        self.property_name = {}

        info = self.get_info()

        self.name = info["name"]
        self.shortname = info["shortname"]
        self.isin = info["isin"]
        self.regnumber = info["regnumber"]
        self.issuesize = info["issuesize"]
        self.facevalue = info["facevalue"]
        self.faceunit = info["faceunit"]
        self.issuedate = info["issuedate"]
        self.latname = info["latname"]
        self.listlevel = info["listlevel"]
        self.isqualifiedinvestors = info["isqualifiedinvestors"]
        self.typename = info["typename"]
        self.group = info["group"]
        self.type = info["type"]
        self.groupname = info["groupname"]
        self.emitter_id = info["emitter_id"]

        self.board_primary = None
        board_primary = self.get_board_primary()
        if board_primary:
            self.board_primary = board_primary
        else:
            raise SecurityError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"Для торгового инструмента {self.secid}," \
                f"не найден приоритный режим торговли"
                )

    def __str__(self):
        """
        ### Метод возвращает строковое представление объекта
        """
        return self.secid

    def get_info(self):
        """
        ### Метод для получения общей информации о ценной бумаге
        """
        if self.secid == "" or self.secid is None:
            raise SecurityError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"Код финансового инструмента не может быть пустым")

        property_dict = {}
        rows = Moex.get_security(security=self.secid)
        if len(rows) == 0:
            raise SecurityError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"Финансовый инструмент с  именем = {self.secid} не найден")

        for row in rows:
            self.property_name[row["name"].lower()] = row["title"]
            property_dict[row["name"].lower()] = row["value"]

        return property_dict

    def get_boards(self):
        """
        ### Метод для получения режимов торгов ценной бумаги
        """
        rows = Moex.get_security(
            block="boards",
            security=self.secid,
        )
        if len(rows) == 0:
            raise SecurityError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"Финансовый инструмент с  именем = {self.secid} не торгуется")

        return rows

    def get_board_primary(self):
        """
        ### Метод для получения основного режима торгов ценной бумаги
        """
        rows = self.get_boards()
        for row in rows:
            if row["is_primary"] == 1:
                return row

    def get_bond_coupons(self):
        """
        ### Метод для получения графика выплаты купонов(только для облигаций)
        """
        rows = Moex.get_security_bondization(
            block="coupons",
            security=self.secid,
        )
        return rows

    def get_bond_amortizations(self):
        """
        ### Метод для получения суммированной доходности по купонам(только для облигаций)
        """
        rows = Moex.get_security_bondization(
            block="amortizations",
            security=self.secid,
        )
        return rows

    def get_bond_yield(
        self,
        date_from: Optional[str] = None,
        date_till: Optional[str] = None,
    ):
        """
        ### Метод для получения истории доходностей(только для облигаций)
        
        Args:
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        """
        rows = Moex.get_history_security_yields(
            engine=self.board_primary["engine"],
            market=self.board_primary["market"],
            yields=self.secid,
            date_from=date_from,
            date_till=date_till,
        )
        return rows

    def get_share_dividends(self):
        """
        ### Метод для получения дивидентов(только для акций)
        """
        rows = Moex.get_security_dividends(security=self.secid)
        return rows

    def get_history_trading_from_board(self,
                                       date_from: Optional[str] = None,
                                       date_till: Optional[str] = None,
                                       columns: Optional[str] = None):
        """
        ### Метод для получения истории торгов по группе режима торгов
        
        Args:
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        """
        rows = Moex.get_history_security_from_board(
            engine=self.board_primary["engine"],
            market=self.board_primary["market"],
            board=self.board_primary["boardid"],
            security=self.secid,
            date_from=date_from,
            date_till=date_till,
            columns=columns)
        return rows
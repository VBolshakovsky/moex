import pandas as pd
from src.moex.moex import Moex
from src.securities.security import Security

# https://jsonformatter.curiousconcept.com/


def convert_to_pandas(j: dict) -> pd.core.frame.DataFrame:
    """
    ### Метод для конвертирования json в Pandas.DataFrame:
    
    Args:
    - j : json, должен быть вида {column1:value, column2:value}
    """
    df = pd.json_normalize(j)
    return df


s = Security("RU000A105WH2")
answer = s.get_history_tradingy_from_board(date_from="2023-04-17",
                                           date_till="2023-04-19",
                                           title_ru=False)

#print(answer)
#print(convert_to_pandas(answer))
from os import environ
from dotenv import load_dotenv

load_dotenv()


class Settings:
    #Общие
    APP_NAME = environ.get('APP_NAME', 'moex_gate')
    TIME_ZONE = environ.get('TIME_ZONE', 'Europe/Moscow')

    #Логирование
    LOG_DATEFMT = environ.get('LOG_DATEFMT', '%d.%m.%Y %H:%M:%S [%Z]')
    LOG_FMT = environ.get(
        'LOG_FMT', '%(asctime)s: [%(name)s|%(levelname)s]: %(message)s')
    LOG_PATH = environ.get('LOG_PATH', 'log')

    #OpenSearch
    OPEN_SEARCH_SERVER = environ.get('OPEN_SEARCH_HOST', 'localhost')
    OPEN_SEARCH_PORT = environ.get('OPEN_SEARCH_PORT', '9200')
    OPEN_SEARCH_HOST = f"{OPEN_SEARCH_SERVER}:{OPEN_SEARCH_PORT}"  #"https://localhost:9200"
    OPEN_SEARCH_INDEX = environ.get('OPEN_SEARCH_INDEX', APP_NAME.lower())
    OPEN_SEARCH_USER = environ.get('OPEN_SEARCH_USER', 'admin')
    OPEN_SEARCH_PASSWORD = environ.get('OPEN_SEARCH_PASSWORD', 'admin')

    #Rabbit
    RMQ_HOST = environ.get('RMQ_HOST', 'localhost')
    RMQ_PORT = environ.get('RMQ_PORT', 5672)
    RMQ_USER = environ.get('RMQ_USER', 'guest')
    RMQ_PASSWORD = environ.get('RMQ_PASSWORD', 'guest')

    #Periodic schedule
    TASK_EVENT_HOURS = environ.get('TASK_EVENT_HOURS', 0)
    TASK_EVENT_MINUTES = environ.get('TASK_EVENT_MINUTES', 0)
    TASK_EVENT_SECONDS = environ.get('TASK_EVENT_SECONDS', 0)

    #Start time schedule
    TASK_STARTTIME_HOURS = environ.get('TASK_STARTTIME_HOURS', 0)
    TASK_STARTTIME_MINUTES = environ.get('TASK_STARTTIME_MINUTES', 0)
    TASK_STARTTIME_SECONDS = environ.get('TASK_STARTTIME_SECONDS', 0)

    #REDIS
    REDIS_HOST = environ.get('REDIS_HOST', 'localhost')
    REDIS_PORT = environ.get('REDIS_PORT', '6379')
    REDIS_PASSWORD = environ.get('REDIS_PASSWORD', '')
    REDIS_DB = environ.get('REDIS_DB', 0)

    HD_EXCHANGE = environ.get('HD_EXCHANGE', None)
    HD_QUEUES = environ.get('HD_QUEUES', '').split(sep=",")
    #Список ценных бумаг(history_trading)
    HD_SECURITIES = environ.get('HD_SECURITIES', '').split(sep=",")
    #Список полей для выгрузки(history_trading)
    HD_FILTER_COLUMNS = environ.get('HD_FILTER_COLUMNS', None)
    #Глубина выдачи данных(в днях)(history_trading)
    HD_COUNT_DAY = int(environ.get('HD_COUNT_DAY', 0))

import logging
from src.service.logger.logger import init_logger
from src.config import Settings as settings
#print(settings.APP_NAME)
#init_logger()
#logger = logging.getLogger(settings.APP_NAME)
#logger.debug('Это отладочное сообщение.')
#logger.info('Это информационное сообщение.')
#logger.warning('Это предупреждающее сообщение.')
#logger.error('Это сообщение об ошибке.')
#logger.critical('Это сообщение о критической ошибке.')

from fastapi import FastAPI
from fastapi import APIRouter
from src.moex.api.router import router_moex_reference
from src.securities.api.router import router_securities

app_api = FastAPI(title=settings.APP_NAME,
                  description="Проект для взаимодействия с Московской биржей",
                  include_function_name=False,
                  add_schema_route=False,
                  prefix="/apiv1")

main_api_router = APIRouter(prefix="/apiv1")

main_api_router.include_router(
    router=router_moex_reference,
    prefix="/moex",
    tags=["Справочники Moex"],
)

main_api_router.include_router(
    router=router_securities,
    prefix="/securities",
    tags=["Ценные бумаги"],
)

app_api.include_router(main_api_router)


@app_api.get("/")
async def root():
    return {"message": "Connect"}


#cd moex_gate\src
#uvicorn main:app_api --reload
# Если как ниже не видит env
#uvicorn moex_gate.src.main:app_api --reload --port 8000
#uvicorn moex_gate.src.main:app_api --reload --port 8000 --host 0.0.0.0 --workers 4
#http://127.0.0.1:8000
#http://127.0.0.1:8000/docs
#http://127.0.0.1:8000/redoc
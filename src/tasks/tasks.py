"""
Description: Celery beat tasks
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 27.06.2023
Links:
    # Описание консольных комманд
    #https://docs.celeryq.dev/en/latest/userguide/monitoring.html#monitoring-control
    # Настройки
    #https://docs.celeryq.dev/en/latest/userguide/configsuration.html
    # Переодические задачи
    #https://docs.celeryq.dev/en/latest/userguide/periodic-tasks.html
"""

from typing import Optional
import logging
import celery
from celery.schedules import crontab
from datetime import datetime, timedelta
from src.service.logger.logger import init_logger
from src.service.rabbit.instance import rabbit
from src.securities.security import Security
from src.config import Settings as settings

broker_redis = f'redis://:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB}'

app_celery = celery.Celery('tasks', broker=broker_redis, backend=broker_redis)
app_celery.conf.timezone = settings.TIME_ZONE
app_celery.autodiscover_tasks()

#Подключаем логер
init_logger()
logger = logging.getLogger(settings.APP_NAME)

# logger.debug('Это отладочное сообщение.')
# logger.info('Это информационное сообщение.')
# logger.warning('Это предупреждающее сообщение.')
# logger.error('Это сообщение об ошибке.')
# logger.critical('Это сообщение о критической ошибке.')

r = rabbit()


def history_trading(securities: list,
                    exchange: str,
                    date_from: Optional[str] = None,
                    date_till: Optional[str] = None,
                    columns: Optional[str] = None) -> list:
    """
    ### Функция для отправки в Rabbit, истории торгов по ценной бумаге
    Если ключа в исходном словаре нет, он пропускается
    
    Args:
    - securities    : Список ценных бумаг, по которым нужна выгрузка
    - exchange      : имя exchange Rabbit, куда отправлять 
    - date_from     : дата, начиная с которой необходимо начать выводить данные
    - date_till     : дата, до которой выводить данные
    - columns       : список полей для выгрузки  
    """
    sent_secid = []

    for security in securities:
        s = Security(security)

        history_trading = s.get_history_trading_from_board(date_from=date_from,
                                                           date_till=date_till,
                                                           columns=columns)
        if history_trading:
            r.publish_message(exchange=exchange,
                              message={
                                  "secid": s.secid,
                                  "shortname": s.shortname,
                                  "type": "history_trading",
                                  "body": history_trading
                              })
            sent_secid.append(s.secid)

    return sent_secid


@app_celery.task
def task_history_trading():
    """
    Функция для запуска периодической задачи celery
    """

    try:
        date_yesterday = datetime.today() - timedelta(days=1)
        date_till = date_yesterday
        date_from = date_till - timedelta(days=settings.HD_COUNT_DAY)

        r.create_structure(exchange_name=settings.HD_EXCHANGE,
                           exchange_type="fanout",
                           queues=settings.HD_QUEUES)

        if settings.HD_SECURITIES:
            sent_secid = history_trading(
                securities=settings.HD_SECURITIES,
                exchange=settings.HD_EXCHANGE,
                date_from=date_from.strftime('%Y-%m-%d'),
                date_till=date_till.strftime('%Y-%m-%d'),
                columns=settings.HD_FILTER_COLUMNS)

            logger.info(
                "По инструментам: %s, выгружена история за период (%s/%s)",
                sent_secid, date_from.strftime('%Y-%m-%d'),
                date_till.strftime('%Y-%m-%d'))

    except Exception as err:
        logger.error("Ошибка task_history_trading: %s.", err)
        raise


app_celery.conf.beat_schedule = {
    'task_history_trading': {
        'task':
        'src.tasks.tasks.task_history_trading',
        'schedule':
        crontab(minute=settings.TASK_STARTTIME_MINUTES,
                hour=settings.TASK_STARTTIME_HOURS)
    }
}

"""
Description: Object Moscow exchange api(iis)
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 20.06.2023
Links:
    Описание методов:
    https://iss.moex.com/iss/reference/
    Руководство разработчика (v.1.4):
    https://fs.moex.com/files/6523
Comment:
    ГГГГ-ММ-ДД - общий формат даты
"""

import pandas as pd
from src.moex.client import URLMoex
from src.moex.client import DataMoex
from typing import Optional
from datetime import datetime


class MoexError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class Moex():
    """
    ### Класс информационно-статистический сервер Московской Биржи (ИСС / ISS)
    """

    TODAY = datetime.today().strftime('%Y-%m-%d')

    @classmethod
    def convert_to_pandas(cls, j: dict) -> pd.core.frame.DataFrame:
        """
        ### Метод для конвертирования json в Pandas.DataFrame:

        Args:
        - j : json, должен быть вида {column1:value, column2:value}
        """
        df = pd.json_normalize(j)
        return df

    @classmethod
    def convert_date(cls, date: datetime = datetime.today()):
        """
        ### Метод для конвертирования даты в формат IIS, по умолчанию возвращает текущую дату

        Args:
        - date  : дата
        """
        return date.strftime('%Y-%m-%d')

    @classmethod
    def replace_title(cls,
                      j: dict,
                      j_title: dict,
                      pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для замены названия полей данных, на заданный словарь
            Выводит данные только по тем ключам, которые есть в j_title
        Args:
        - j         : входящий json
        - j_title   : словарь для замены вида {name:имя,}
        - pandas    : выводить данные в Pandas.DataFrame
        """
        replace = []
        j_replace = {}

        for row in j:
            for title in j_title:
                # Проверим наличие ключа из (j_title), в словаре с данными(j)
                if title["name"].lower() in row:
                    j_replace[title["short_title"]] = row[
                        title["name"].lower()]
            replace.append(j_replace)

        if pandas:
            return Moex.convert_to_pandas(j=replace)
        return replace

    @classmethod
    def get_reference(cls,
                      block: str = "engines",
                      columns: Optional[str] = None,
                      pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения справочников IIS
            https://iss.moex.com/iss/reference/28
            
        Args:
        - block     : имя блока, по которому предполагается получения данных
            - engines               / торговые системы
            - markets               / рынки
            - boards                / режимы торговли
            - boardgroups           / группы режимов торговли
            - durations             / расчетные интервалы свечей в формате HLOCV
            - securitytypes         / типы ценных бумаг
            - securitygroups        / группы ценных бумаг
            - securitycollections   / колекции ценных бумаг
        - columns   : какие колонки у блока нужно выгрузить, "column1,column2"
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        http://iss.moex.com/iss/index.json
        """

        url_moex = URLMoex(url_tail="index",
                           extension="json",
                           iss_meta=False,
                           iss_only=block,
                           iss_columns={block: columns.upper()}
                           if columns is not None else columns)

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer[block])

        return answer[block]

    @classmethod
    def get_engines(cls,
                    pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения доступных торговых систем IIS
            https://iss.moex.com/iss/reference/40
            - stock: Фондовый рынок и рынок депозитов
            - state: Рынок ГЦБ (размещение)
            - currency: Валютный рынок
            - futures: Срочный рынок
            - commodity: Товарный рынок
            - interventions: Товарные интервенции
            - offboard: ОТС-система
            - agro: Агро
            - otc: ОТС с ЦК
            - quotes: Квоты
            
        Args:
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        http://iss.moex.com/iss/engines.json
        """

        url_moex = URLMoex(
            url_tail="engines",
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer["engines"])

        return answer["engines"]

    @classmethod
    def get_engine(cls,
                   block: str = "timetable",
                   engine: str = "stock",
                   pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения информации о торговой системе IIS
            https://iss.moex.com/iss/reference/41
            
        Args:
        - block     : имя блока, по которому предполагается получения данных
            - engines   / торговые системы
            - timetable / недельное расписание работы
            - dailytable/ работа вне недельного расписания, 
                          например в праздничные или предпраздничные дни
        - engine    : торговая система
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            extension="json",
            iss_meta=False,
            iss_only=block,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer[block])

        return answer[block]

    @classmethod
    def check_workday(cls, date: datetime, engine: str = "stock") -> bool:
        """
        ### Метод для проверки даты, велись ли в этот день торги
            
        Args:
        - engine    : торговая система
        """

        week_days = Moex.get_engine(block="timetable", engine=engine)
        dailytable = Moex.get_engine(block="dailytable", engine=engine)

        except_days = []
        for except_day in dailytable:
            except_days.append({
                "date":
                datetime.strptime(except_day["date"], '%Y-%m-%d'),
                "is_work_day":
                except_day["is_work_day"]
            })

        # Проверим в исключительных днях
        for except_day in except_days:
            if date == except_day["date"]:
                if except_day["is_work_day"] == 1:
                    return True
                if except_day["is_work_day"] == 0:
                    return False

        # Проверим по дню недели в основном графике
        for week_day in week_days:
            if week_day["week_day"] == (date.weekday() + 1):
                if week_day["is_work_day"] == 1:
                    return True

        return False

    @classmethod
    def get_markets(cls,
                    engine: str = "stock",
                    pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения доступных рынков для торговой системы
            https://iss.moex.com/iss/reference/42
            - index: Индексы фондового рынка
            - shares: Рынок акций
            - bonds: Рынок облигаций

        Args:
        - engine    : торговая система
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_tail="markets",
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer["markets"])

        return answer["markets"]

    @classmethod
    def get_market(cls,
                   block: str = "boards",
                   engine: str = "stock",
                   market: str = "shares",
                   pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения информации о рынке IIS
            https://iss.moex.com/iss/reference/44

        Args:
        - block     : имя блока, по которому предполагается получения данных
            - boards                        / Доступные режимы торгов рынка
            - boardgroups                   / Группы режимов торгов
            - securities                    / Справочник полей таблицы со статическими данными торговой сессии рынка
            - marketdata                    / Справочник полей таблицы котировок
            - trades                        / Справочник полей таблицы сделок торговой сессии рынка
            - orderbook                     / Справочник полей таблицы с котировками (стакана заявок)
            - history                       / Справочник полей истории таблицы инструментов
            - trades_hist marketdata_yields / Справочник полей таблицы сделок архив реестра сделок
            - trades_yields                 / ?
            - history_yields                / ?
            - secstats                      / Описание полей таблицы "Итоги для "
        - engine    : торговая система
        - market    : рынок
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_market=market,
            extension="json",
            iss_meta=False,
            iss_only=block,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer[block])

        return answer[block]

    @classmethod
    def get_boards(cls,
                   engine: str = "stock",
                   market: str = "shares",
                   is_traded: Optional[bool] = None,
                   pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения доступных режимов торгов рынка
            https://iss.moex.com/iss/reference/43
            - TQBR: Т+: Акции и ДР - безадрес.
            - TQCB: Т+: Облигации - безадрес.
            
        Args:
        - engine    : торговая система
        - market    : рынок
        - is_traded : торгуемые(true/false/all)
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boards.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_market=market,
            url_tail="boards",
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        answer_traded = []

        if is_traded is not None:
            for board in answer["boards"]:
                if is_traded:
                    if board["is_traded"] == 1:
                        answer_traded.append(board)
                else:
                    if board["is_traded"] == 0:
                        answer_traded.append(board)
            answer["boards"] = answer_traded

        if pandas:
            return Moex.convert_to_pandas(j=answer["boards"])

        return answer["boards"]

    @classmethod
    def get_board(cls,
                  engine: str = "stock",
                  market: str = "shares",
                  board: str = "TQBR",
                  pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения информации о режиме торгов рынка
            https://iss.moex.com/iss/reference/49
            - TQBR: Т+: Акции и ДР - безадрес.
            - TQCB: Т+: Облигации - безадрес.
            
        Args:
        - engine    : торговая система
        - market    : рынок
        - board     : режим торгов
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_market=market,
            url_board=board,
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer["board"])

        return answer["board"]

    @classmethod
    def get_security(cls,
                     block: str = "description",
                     security: str = "AFLT",
                     pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения информации о ценной бумаге
            https://iss.moex.com/iss/reference/13
            - TQBR: Т+: Акции и ДР - безадрес.
            - TQCB: Т+: Облигации - безадрес.
            
        Args:
        - block     : имя блока, по которому предполагается получения данных
            - description   / Описание инструмента
            - boards        / Режимы, на которых торгуется инструмент
        - security  : код ценной бумаги
        - pandas    : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities/AFLT.json
        """

        url_moex = URLMoex(url_security=security,
                           extension="json",
                           iss_meta=False,
                           iss_only=block)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer[block])
        return answer[block]

    @classmethod
    def get_security_indices(
            cls,
            security: str = "AFLT",
            only_actual: Optional[bool] = None,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения списка индексов в которые входит бумага
            https://iss.moex.com/iss/reference/160

        Args:
        - security      : код ценной бумаги
        - only_actual   : выводить индексы в базе которого бумага находится прямо сейчас
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities/AFLT/indices.json
        """

        option = {}
        if only_actual is not None and only_actual:
            option["only_actual"] = 1
        else:
            option["only_actual"] = 0

        url_moex = URLMoex(url_security=security,
                           url_tail="indices",
                           extension="json",
                           option=option,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer["indices"])
        return answer["indices"]

    @classmethod
    def get_security_aggregates(
            cls,
            security: str = "AFLT",
            date: Optional[bool] = None,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения агрегированных итогов торгов за дату по рынкам
            На сайте называется "Объемы торгов"
            https://iss.moex.com/iss/reference/214

        Args:
        - security      : код ценной бумаги
        - date          : дата за которую необходимо отобразить данные.
                        По умолчанию за последнюю дату в итогах торгов.
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities/AFLT/aggregates.json
        """

        option = {}
        if date:
            option["date"] = date

        url_moex = URLMoex(url_security=security,
                           url_tail="aggregates",
                           extension="json",
                           option=option,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer["aggregates"])
        return answer["aggregates"]

    @classmethod
    def get_security_from_board(
            cls,
            block: str = "securities",
            engine: str = "stock",
            market: str = "shares",
            board: str = "TQBR",
            security: str = "AFLT",
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения данных по указанному инструменту на выбранном режиме торгов
            https://iss.moex.com/iss/reference/53

        Args:
        - block         : имя блока, по которому предполагается получения данных
            - securities   / Справочник полей таблицы со статическими данными торговой сессии рынка
            - marketdata   / Данные с текущими значениями инструментов рынка
        - engine        : торговая система
        - market        : рынок
        - board         : режим торгов
        - security      : код ценной бумаги
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities/AFLT.json
        https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities/columns.json
        """

        if name_columns:
            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_tail="securities",
                               url_columns=True,
                               extension="json",
                               iss_meta=False,
                               iss_only=block,
                               iss_columns={block: "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer[block])
            return answer[block]
        else:

            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_board=board,
                               url_security=security,
                               iss_meta=False,
                               extension="json",
                               iss_only=block)

            request = DataMoex(url_moex.url)
            answer = request.get_data()

            if pandas:
                return Moex.convert_to_pandas(j=answer[block])

            return answer[block]

    @classmethod
    def get_securities_from_market(
            cls,
            block: str = "securities",
            engine: str = "stock",
            market: str = "shares",
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения таблицы инструментов торговой сессии по рынку в целом
            https://iss.moex.com/iss/reference/33

        Args:
        - block         : имя блока, по которому предполагается получения данных
            - securities   / Справочник полей таблицы со статическими данными торговой сессии рынка
            - marketdata   / Данные с текущими значениями инструментов рынка
        - engine        : торговая система
        - market        : рынок
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/securities.json
        https://iss.moex.com/iss/engines/stock/markets/shares/securities/columns.json
        """

        if name_columns:
            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_tail="securities",
                               url_columns=True,
                               extension="json",
                               iss_meta=False,
                               iss_only=block,
                               iss_columns={block: "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer[block])

            return answer[block]

        else:
            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_tail="securities",
                               extension="json",
                               iss_meta=False,
                               iss_only=block)
            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer[block])

            return answer[block]

    @classmethod
    def get_security_from_market(
            cls,
            block: str = "securities",
            engine: str = "stock",
            market: str = "shares",
            security: str = "AFLT",
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения данных по конкретному инструменту рынка
            https://iss.moex.com/iss/reference/52

        Args:
        - block         : имя блока, по которому предполагается получения данных
            - securities   / Справочник полей таблицы со статическими данными торговой сессии рынка
            - marketdata   / Данные с текущими значениями инструментов рынка
        - engine        : торговая система
        - market        : рынок
        - security      : код ценной бумаги
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/securities/AFLT.json
        https://iss.moex.com/iss/engines/stock/markets/shares/securities/columns.json
        """

        if name_columns:
            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_security=security,
                               url_columns=True,
                               extension="json",
                               iss_meta=False,
                               iss_only=block,
                               iss_columns={block: "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer[block])

            return answer[block]

        else:
            url_moex = URLMoex(url_engine=engine,
                               url_market=market,
                               url_security=security,
                               extension="json",
                               iss_meta=False,
                               iss_only=block)
            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer[block])

            return answer[block]

    @classmethod
    def get_listing_from_market(
            cls,
            engine: str = "stock",
            market: str = "shares",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения списка неторгуемых инструментов с указанием интервалов торгуемости по режимам
            https://iss.moex.com/iss/reference/118
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/listing.json
        """

        url_moex = URLMoex(url_head="history",
                           url_engine=engine,
                           url_market=market,
                           url_tail="listing",
                           extension="json",
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data(blockname_parts=["securities"])

        if pandas:
            return Moex.convert_to_pandas(j=answer["securities"])
        return answer["securities"]

    @classmethod
    def get_listing_from_board(
            cls,
            engine: str = "stock",
            market: str = "shares",
            board: str = "TQBR",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения данных по листингу бумаг в историческом разрезе по указанному режиму
            https://iss.moex.com/iss/reference/119
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - board         : режим торгов
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/listing.json
        """

        url_moex = URLMoex(url_head="history",
                           url_engine=engine,
                           url_market=market,
                           url_board=board,
                           url_tail="listing",
                           extension="json",
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data(blockname_parts=["securities"])

        if pandas:
            return Moex.convert_to_pandas(j=answer["securities"])
        return answer["securities"]

    @classmethod
    def get_boardgroups(
            cls,
            engine: str = "stock",
            market: str = "shares",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения справочника групп режимов торгов по рынку
            https://iss.moex.com/iss/reference/45
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boardgroups.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_market=market,
            url_tail="boardgroups",
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer["boardgroups"])

        return answer["boardgroups"]

    @classmethod
    def get_boardgroup(cls,
                       engine: str = "stock",
                       market: str = "shares",
                       boardgroup: str = "57",
                       pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения описания группы режимов торгов
            https://iss.moex.com/iss/reference/50
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - boardgroup    : группа режимов торговли
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boardgroups/57.json
        """

        url_moex = URLMoex(
            url_engine=engine,
            url_market=market,
            url_boardgroups=boardgroup,
            extension="json",
            iss_meta=False,
        )

        request = DataMoex(url_moex.url)
        answer = request.get_data()
        if pandas:
            return Moex.convert_to_pandas(j=answer["boardgroup"])

        return answer["boardgroup"]

    @classmethod
    def get_security_from_boardgroup(
            cls,
            block: str = "securities",
            engine: str = "stock",
            market: str = "shares",
            boardgroup: str = "57",
            security: str = "AFLT",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения описания группы режимов торгов
            https://iss.moex.com/iss/reference/50
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - boardgroup    : группа режимов торговли
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boardgroups/57.json
        """

        url_moex = URLMoex(url_engine=engine,
                           url_market=market,
                           url_boardgroups=boardgroup,
                           url_security=security,
                           extension="json",
                           iss_only=block,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer[block])
        return answer[block]

    @classmethod
    def get_history_security_yields(
            cls,
            engine: str = "stock",
            market: str = "bonds",
            yields: str = "RU000A105WH2",
            sort_order: Optional[str] = None,
            date_from: Optional[str] = None,
            date_till: Optional[str] = None,
            numtrades: Optional[int] = None,
            lang: Optional[str] = None,
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории доходностей по одной бумаге на рынке
            https://iss.moex.com/iss/reference/793
            Только для облигаций
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - yields        : код ценной бумаги
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        - numtrades     : минимальное количество сделок с бумагой
        - lang          : язык результата: ru или en
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/bonds/yields/RU000A105WH2.json
        https://iss.moex.com/iss/history/engines/stock/markets/bonds/yields/columns.json
        """

        if name_columns:
            url_moex = URLMoex(
                url_head="history",
                url_engine=engine,
                url_market=market,
                url_tail="yields",
                url_columns=True,
                extension="json",
                iss_columns={"history_yields": "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer["history_yields"])

            return answer["history_yields"]

        else:
            option = {}

            if sort_order:
                option["sort_order"] = sort_order
            if date_from:
                option["from"] = date_from
            if date_till:
                option["till"] = date_till
            if numtrades:
                option["numtrades"] = numtrades
            if lang:
                option["lang"] = lang

            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_yields=yields,
                               extension="json",
                               option=option,
                               iss_meta=False)

            request = DataMoex(url_moex.url)
            answer = request.get_data()

            if pandas:
                return Moex.convert_to_pandas(j=answer["history_yields"])
            return answer["history_yields"]

    @classmethod
    def get_history_security_from_boardgroup(
            cls,
            engine: str = "stock",
            market: str = "shares",
            boardgroup: str = "57",
            security: str = "AFLT",
            sort_order: Optional[str] = None,
            date_from: Optional[str] = None,
            date_till: Optional[str] = None,
            numtrades: Optional[int] = None,
            lang: Optional[str] = None,
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории торгов для указанной бумаги по группе режима торгов
            https://iss.moex.com/iss/reference/153
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - yields        : код ценной бумаги
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        - numtrades     : минимальное количество сделок с бумагой
        - lang          : язык результата: ru или en
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/boardgroups/57/securities/AFLT.json
        https://iss.moex.com/iss/history/engines/stock/markets/shares/boardgroups/57/securities/columns.json
        """

        option = {}

        if sort_order:
            option["sort_order"] = sort_order
        if date_from:
            option["from"] = date_from
        if date_till:
            option["till"] = date_till
        if numtrades:
            option["numtrades"] = numtrades
        if lang:
            option["lang"] = lang

        if name_columns:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_boardgroups=boardgroup,
                               url_tail="securities",
                               url_columns=True,
                               extension="json",
                               iss_meta=False,
                               iss_columns={"history": "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])

            return answer["history"]
        else:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_boardgroups=boardgroup,
                               url_security=security,
                               extension="json",
                               option=option,
                               iss_meta=False)

            request = DataMoex(url_moex.url)
            answer = request.get_data()

            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])
            return answer["history"]

    @classmethod
    def get_history_security_from_market(
            cls,
            engine: str = "stock",
            market: str = "shares",
            security: str = "AFLT",
            sort_order: Optional[str] = None,
            date_from: Optional[str] = None,
            date_till: Optional[str] = None,
            numtrades: Optional[int] = None,
            lang: Optional[str] = None,
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории торгов для указанной бумаги по рынку
            https://iss.moex.com/iss/reference/63
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - security      : код ценной бумаги
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        - numtrades     : минимальное количество сделок с бумагой
        - lang          : язык результата: ru или en
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/securities/AFLT.json
        https://iss.moex.com/iss/history/engines/stock/markets/shares/securities/columns.json
        """

        option = {}

        if sort_order:
            option["sort_order"] = sort_order
        if date_from:
            option["from"] = date_from
        if date_till:
            option["till"] = date_till
        if numtrades:
            option["numtrades"] = numtrades
        if lang:
            option["lang"] = lang

        if name_columns:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_tail="securities",
                               url_columns=True,
                               extension="json",
                               iss_meta=False,
                               iss_columns={"history": "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])

            return answer["history"]
        else:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_security=security,
                               extension="json",
                               option=option,
                               iss_meta=False)

            request = DataMoex(url_moex.url)
            answer = request.get_data()

            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])
            return answer["history"]

    @classmethod
    def get_history_security_from_board(
            cls,
            engine: str = "stock",
            market: str = "shares",
            board: str = "TQBR",
            security: str = "AFLT",
            columns: Optional[str] = None,
            sort_order: Optional[str] = None,
            date_from: Optional[str] = None,
            date_till: Optional[str] = None,
            numtrades: Optional[int] = None,
            lang: Optional[str] = None,
            name_columns: bool = False,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории торгов для указанной бумаги по режиму торгов
            https://iss.moex.com/iss/reference/65
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - board         : режим торгов
        - security      : код ценной бумаги
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        - numtrades     : минимальное количество сделок с бумагой
        - lang          : язык результата: ru или en
        - name_columns  : вывести описание полей на русском
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/AFLT.json
        https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/columns.json
        """

        option = {}

        if sort_order:
            option["sort_order"] = sort_order
        if date_from:
            option["from"] = date_from
        if date_till:
            option["till"] = date_till
        if numtrades:
            option["numtrades"] = numtrades
        if lang:
            option["lang"] = lang

        if name_columns:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_board=board,
                               url_tail="securities",
                               url_columns=True,
                               extension="json",
                               option=option,
                               iss_meta=False,
                               iss_columns={"history": "name,short_title"})

            request = DataMoex(url_moex.url)
            answer = request.get_data()
            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])

            return answer["history"]
        else:
            url_moex = URLMoex(url_head="history",
                               url_engine=engine,
                               url_market=market,
                               url_board=board,
                               url_security=security,
                               extension="json",
                               option=option,
                               iss_columns={"history": columns.upper()}
                               if columns is not None else columns,
                               iss_meta=False)

            request = DataMoex(url_moex.url)
            answer = request.get_data()

            if pandas:
                return Moex.convert_to_pandas(j=answer["history"])
            return answer["history"]

    @classmethod
    def get_security_dividends(
            cls,
            security: str = "AFLT",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения дивидентов по акции
            Скрытый api IIS, только для акций
            
        Args:
        - security      : код ценной бумаги
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities/AFLT/dividends.json
        """

        url_moex = URLMoex(url_security=security,
                           url_tail="dividends",
                           extension="json",
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer["dividends"])
        return answer["dividends"]

    @classmethod
    def get_security_bondization(
            cls,
            block: str = "coupons",
            security: str = "RU000A105WH2",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения доходности облигаций(купоны)
            Скрытый api IIS, только для облигаций
            
        Args:
        - block         : имя блока, по которому предполагается получения данных
            - amortizations / Общая доходность(сумирование coupons)
            - coupons       / Выплаты по купонам
            - offers        / ?
        - security      : код ценной бумаги
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities/RU000A105WH2/bondization.json
        """

        url_moex = URLMoex(url_security=security,
                           url_tail="bondization",
                           extension="json",
                           iss_only=block,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer[block])
        return answer[block]

    @classmethod
    def get_security_candleborders(
            cls,
            block: str = "borders",
            engine: str = "stock",
            market: str = "shares",
            board: str = "TQBR",
            security: str = "AFLT",
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения периода дат рассчитанных свечей
            https://iss.moex.com/iss/reference/48
            
        Args:
        - block         : имя блока, по которому предполагается получения данных
            - borders   / Период доступных свечей
            - durations / Справочник доступных расчетных интервалов свечей в формате HLOCV
        - engine        : торговая система
        - market        : рынок
        - board         : режим торгов
        - security      : код ценной бумаги
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities/AFLT/candleborders.json
        """

        url_moex = URLMoex(url_engine=engine,
                           url_market=market,
                           url_board=board,
                           url_security=security,
                           url_tail="candleborders",
                           extension="json",
                           iss_only=block,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer[block])
        return answer[block]

    @classmethod
    def get_security_candles(
            cls,
            engine: str = "stock",
            market: str = "shares",
            board: str = "TQBR",
            security: str = "AFLT",
            reverse: Optional[bool] = None,
            date_from: Optional[str] = None,
            date_till: Optional[str] = None,
            interval: Optional[int] = None,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения свечи указанного инструмента по выбранному режиму торгов
            https://iss.moex.com/iss/reference/46
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - board         : режим торгов
        - security      : код ценной бумаги
        - reverse       : обратная сортировка
        - date_from     : дата, начиная с которой необходимо начать выводить данные
        - date_till     : дата, до которой выводить данные
        - interval      : интервал графика
            - 1   / минута
            - 10  / минут
            - 60  / час
            - 24  / день
            - 7   / неделя
            - 31  / месяц
            - 4   / квартал
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities/AFLT/candles.json
        """
        option = {}

        if reverse is not None and reverse:
            option["iss.reverse"] = "true"
        else:
            option["iss.reverse"] = "false"
        if date_from:
            option["from"] = date_from
        if date_till:
            option["till"] = date_till
        if interval:
            option["interval"] = interval

        url_moex = URLMoex(url_engine=engine,
                           url_market=market,
                           url_board=board,
                           url_security=security,
                           url_tail="candles",
                           extension="json",
                           option=option,
                           iss_only="candles",
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data(blockname_parts=[
            "candles",
        ])

        if pandas:
            return Moex.convert_to_pandas(j=answer["candles"])
        return answer["candles"]

    @classmethod
    def get_history_securities_from_date(
            cls,
            engine: str = "stock",
            market: str = "shares",
            sort_order: Optional[str] = None,
            date: Optional[str] = None,
            numtrades: Optional[int] = None,
            sort_column: Optional[str] = None,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории по всем бумагам на рынке за одну дату
            https://iss.moex.com/iss/reference/62
            Выдается частями
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date          : дата за которую необходимо вывести данные. Формат: ГГГГ-ММ-ДД
        - numtrades     : минимальное количество сделок с бумагой
        - sort_column   : Поле по которому сортируются выдача данных(по умолчанию secid)
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/shares/securities.json?date=2023-06-01
        """

        option = {}

        if sort_order:
            option["sort_order"] = sort_order
        if date:
            option["date"] = date
        if numtrades:
            option["numtrades"] = numtrades
        if sort_column:
            option["sort_column"] = sort_column

        url_moex = URLMoex(url_head="history",
                           url_engine=engine,
                           url_market=market,
                           url_tail="securities",
                           extension="json",
                           option=option,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data()

        if pandas:
            return Moex.convert_to_pandas(j=answer["history"])
        return answer["history"]

    @classmethod
    def get_history_marketyields_from_date(
            cls,
            engine: str = "stock",
            market: str = "bonds",
            sort_order: Optional[str] = None,
            date: Optional[str] = None,
            numtrades: Optional[int] = None,
            sort_column: Optional[str] = None,
            pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения истории рассчитанных доходностей для всех бумаг на указанном режиме торгов
            https://iss.moex.com/iss/reference/791
            Выдается частями, только для облигаций(bonds)
            
        Args:
        - engine        : торговая система
        - market        : рынок
        - sort_order    : направление сортировки
            - "asc"     / По возрастанию значения
            - "desc"    / По убыванию
        - date          : дата за которую необходимо вывести данные. Формат: ГГГГ-ММ-ДД
        - numtrades     : минимальное количество сделок с бумагой
        - sort_column   : Поле по которому сортируются выдача данных(по умолчанию secid)
        - pandas        : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/history/engines/stock/markets/bonds/yields.json
        """

        option = {}

        if sort_order:
            option["sort_order"] = sort_order
        if date:
            option["date"] = date
        if numtrades:
            option["numtrades"] = numtrades
        if sort_column:
            option["sort_column"] = sort_column

        url_moex = URLMoex(url_head="history",
                           url_engine=engine,
                           url_market=market,
                           url_tail="yields",
                           extension="json",
                           option=option,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data(blockname_parts="history_yields")

        if pandas:
            return Moex.convert_to_pandas(j=answer["history_yields"])
        return answer["history_yields"]

    @classmethod
    def get_securities(cls,
                       q: Optional[str] = None,
                       engine: Optional[str] = None,
                       market: Optional[str] = None,
                       is_trading: Optional[bool] = None,
                       group_by: Optional[str] = None,
                       group_by_filter: Optional[str] = None,
                       lang: Optional[str] = None,
                       pandas: bool = False) -> dict | pd.core.frame.DataFrame:
        """
        ### Метод для получения списка бумаг торгуемых на московской бирже
            https://iss.moex.com/iss/reference/5
            Выдается частями(может быть очень много записей), подумать о ассинхронности
            
        Args:
        - q                 : поиск инструментов по части Кода, Названию, ISIN,
                            Идентификатору Эмитента, Номеру гос.регистрации.
                            Слова длиной менее трёх букв игнорируются. 
                            Если параметром передано два слова через пробел. 
                            То каждое должно быть длиной не менее трёх букв.
        - engine            : поиск инструментов по торговой системе
        - market            : поиск инструментов по рынку
        - is_trading        : торгуются?
        - group_by          : доступны значения group и type
            - type  / https://iss.moex.com/iss/securitytypes.json
            - group / https://iss.moex.com/iss/securitygroups.json
        - group_by_filter   : фильтровать по типам group или type. Зависит от значения фильтра group_by
        - lang              : язык результата: ru или en
        - pandas            : выводить данные в Pandas.DataFrame
        
        Example:
        https://iss.moex.com/iss/securities.json
        """

        option = {}
        if q:
            option["q"] = q
        if engine:
            option["engine"] = engine
        if market:
            option["market"] = market
        if is_trading is not None and is_trading:
            option["is_trading"] = 1
        else:
            option["is_trading"] = 0
        if group_by:
            option["group_by"] = group_by
        if group_by_filter:
            option["group_by_filter"] = group_by_filter
        if lang:
            option["lang"] = lang

        url_moex = URLMoex(url_tail="securities",
                           extension="json",
                           option=option,
                           iss_meta=False)

        request = DataMoex(url_moex.url)
        answer = request.get_data(blockname_parts="securities")

        if pandas:
            return Moex.convert_to_pandas(j=answer["securities"])
        return answer["securities"]
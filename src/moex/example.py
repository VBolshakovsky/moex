"""
Description: Example work moex
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 30.06.2023
Links:

"""
from src.moex.client import URLMoex
from src.moex.client import DataMoex
from src.moex.moex import Moex

# Базовый пример работы с клиентом
url_moex = URLMoex(
    url_head="history",
    url_engine="stock",
    url_market="shares",
    url_board="TQBR",
    url_security="YAKG",
    #url_tail="index",
    extension="json",
    iss_meta=False,
    #iss_data=True,
    #iss_only="markets,engines",
    #iss_columns={
    #    "engines": "id,name",
    #    "markets": "id,market_name"
    #},
    option={
        "sort_order": "desc",
        "from": "2023-06-15",
    },
)
#print(url_moex)
request = DataMoex(url=url_moex.url)
answer = request.get_data()
#print(answer)

# Замена названия полей данных, на заданный словарь
# print(
# Moex.replace_title(
# j=Moex.get_security_from_board(
# block="securities",
# market="bonds",
# security="RU000A105WH2",
# board="TQCB",
# #pandas=True,
# name_columns=False),
# j_title=Moex.get_security_from_board(name_columns=True),
# pandas=False))

# Получение агрегированных итогов торгов за дату по рынкам
# print(Moex.get_security_aggregates(security="RU000A105WH2", pandas=True))

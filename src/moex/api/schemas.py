from src.moex.moex import Moex
import enum


def get_list_engines(online: bool = False):
    """
    ### Функция для получения списка торговых систем
    
    Args:
    - online   : получения данных, с api IIS
    """
    list_engines = []

    if online:
        engines = Moex.get_reference(block="engines")

        for engine in engines:
            list_engines.append((engine["name"], engine["name"]))
    else:
        list_engines = [('stock', 'stock'), ('state', 'state'),
                        ('currency', 'currency'), ('futures', 'futures'),
                        ('commodity', 'commodity'),
                        ('interventions', 'interventions'),
                        ('offboard', 'offboard'), ('agro', 'agro'),
                        ('otc', 'otc'), ('quotes', 'quotes')]

    return list_engines


def get_list_markets(online: bool = False):
    """
    ### Функция для получения списка рынков
    
    Args:
    - online   : получения данных, с api IIS
    """

    list_markets = []

    if online:
        markets = Moex.get_reference(block="markets")

        for market in markets:
            if (market["market_name"],
                    market["market_name"]) not in list_markets:
                list_markets.append(
                    (market["market_name"], market["market_name"]))
    else:
        list_markets = [
            ('index', 'index'), ('shares', 'shares'), ('bonds', 'bonds'),
            ('ndm', 'ndm'), ('otc', 'otc'), ('ccp', 'ccp'),
            ('deposit', 'deposit'), ('repo', 'repo'), ('qnv', 'qnv'),
            ('mamc', 'mamc'), ('foreignshares', 'foreignshares'),
            ('foreignndm', 'foreignndm'), ('moexboard', 'moexboard'),
            ('gcc', 'gcc'), ('credit', 'credit'), ('selt', 'selt'),
            ('futures', 'futures'), ('main', 'main'), ('forts', 'forts'),
            ('options', 'options'), ('fortsiqs', 'fortsiqs'),
            ('optionsiqs', 'optionsiqs'), ('sharesndm', 'sharesndm'),
            ('nonresndm', 'nonresndm'), ('nonresrepo', 'nonresrepo'),
            ('nonresccp', 'nonresccp'), ('grain', 'grain'), ('sugar', 'sugar'),
            ('auctions', 'auctions'), ('buyauctions', 'buyauctions'),
            ('standard', 'standard'), ('classica', 'classica')
        ]

    return list_markets


def get_list_securitytypes(online: bool = False):
    """
    ### Функция для получения списка типа ценных бумаг
    
    Args:
    - online   : получения данных, с api IIS
    """
    list_securitytypes = []

    if online:
        securitytypes = Moex.get_reference(block="securitytypes")
        for engine in securitytypes:
            list_securitytypes.append(
                (engine["security_type_name"], engine["security_type_name"]))
    else:
        list_securitytypes = [('common_share', 'common_share'),
                              ('preferred_share', 'preferred_share'),
                              ('depositary_receipt', 'depositary_receipt'),
                              ('ofz_bond', 'ofz_bond'), ('cb_bond', 'cb_bond'),
                              ('subfederal_bond', 'subfederal_bond'),
                              ('municipal_bond', 'municipal_bond'),
                              ('corporate_bond', 'corporate_bond'),
                              ('exchange_bond', 'exchange_bond'),
                              ('ifi_bond', 'ifi_bond'),
                              ('euro_bond', 'euro_bond'),
                              ('public_ppif', 'public_ppif'),
                              ('interval_ppif', 'interval_ppif'),
                              ('rts_index', 'rts_index'),
                              ('private_ppif', 'private_ppif'),
                              ('stock_mortgage', 'stock_mortgage'),
                              ('etf_ppif', 'etf_ppif'),
                              ('stock_index', 'stock_index'),
                              ('exchange_ppif', 'exchange_ppif'),
                              ('stock_index_ci', 'stock_index_ci'),
                              ('stock_index_eq', 'stock_index_eq'),
                              ('stock_index_im', 'stock_index_im'),
                              ('stock_index_fi', 'stock_index_fi'),
                              ('stock_index_ie', 'stock_index_ie'),
                              ('stock_index_mx', 'stock_index_mx'),
                              ('stock_index_if', 'stock_index_if'),
                              ('stock_index_namex', 'stock_index_namex'),
                              ('stock_index_rusfar', 'stock_index_rusfar'),
                              ('stock_index_pf', 'stock_index_pf'),
                              ('stock_deposit', 'stock_deposit'),
                              ('non_exchange_bond', 'non_exchange_bond'),
                              ('state_bond', 'state_bond'),
                              ('currency_index', 'currency_index'),
                              ('currency', 'currency'),
                              ('gold_metal', 'gold_metal'),
                              ('silver_metal', 'silver_metal'),
                              ('currency_futures', 'currency_futures'),
                              ('currency_fixing', 'currency_fixing'),
                              ('currency_wap', 'currency_wap'),
                              ('futures', 'futures'), ('option', 'option'),
                              ('option_on_shares', 'option_on_shares')]

    return list_securitytypes


ModelSecuritytypes = enum.Enum(
    value='securitytypes',
    names=get_list_securitytypes(online=False),
)

ModelEngines = enum.Enum(
    value='engines',
    names=get_list_engines(online=False),
)

ModelMarkets = enum.Enum(
    value='markets',
    names=get_list_markets(online=True),
)

ModelLang = enum.Enum(
    value='lang',
    names=[('ru', 'ru'), ('en', 'en')],
)

ModelGroupe_by = enum.Enum(
    value='group_by',
    names=[('group', 'group'), ('type', 'type')],
)

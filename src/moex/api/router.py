from typing import Optional
from fastapi import APIRouter
from src.moex.api.schemas import ModelEngines
from src.moex.api.schemas import ModelMarkets
from src.moex.moex import Moex

router_moex_reference = APIRouter(prefix="/reference")


@router_moex_reference.get("/engines")
def moex_reference_engines():
    """
    ### Метод для получения доступных торговых систем IIS
        https://iss.moex.com/iss/reference/40
        - stock: Фондовый рынок и рынок депозитов
        - state: Рынок ГЦБ (размещение)
        - currency: Валютный рынок
        - futures: Срочный рынок
        - commodity: Товарный рынок
        - interventions: Товарные интервенции
        - offboard: ОТС-система
        - agro: Агро
        - otc: ОТС с ЦК
        - quotes: Квоты

    Example:
    http://iss.moex.com/iss/engines.json
    """
    return Moex.get_engines()


@router_moex_reference.get("/engines/{engine}")
def moex_reference_engine(engine: ModelEngines):
    """
    ### Метод для получения информации о торговой системе IIS
        https://iss.moex.com/iss/reference/41

    Example:
    https://iss.moex.com/iss/engines/stock.json
    """
    return Moex.get_engine(block="engine", engine=engine.value)


@router_moex_reference.get("/engines/{engine}/timetable")
def moex_reference_engine_timetable(engine: ModelEngines):
    """
    ### Метод для получения информации о недельном расписаним работы
        https://iss.moex.com/iss/reference/41

    Example:
    https://iss.moex.com/iss/engines/stock.json
    """
    return Moex.get_engine(block="timetable", engine=engine.value)


@router_moex_reference.get("/engines/{engine}/dailytable")
def moex_reference_engine_dailytable(engine: ModelEngines):
    """
    ### Метод для получения информации о работе вне недельного расписания
    например в праздничные или предпраздничные дни
        https://iss.moex.com/iss/reference/41

    Example:
    https://iss.moex.com/iss/engines/stock.json
    """
    return Moex.get_engine(block="dailytable", engine=engine.value)


@router_moex_reference.get("/engines/{engine}/markets")
def moex_reference_engine_markets(engine: ModelEngines):
    """
    ### Метод для получения доступных рынков для торговой системы
        https://iss.moex.com/iss/reference/42
        - index: Индексы фондового рынка
        - shares: Рынок акций
        - bonds: Рынок облигаций

    Example:
    https://iss.moex.com/iss/engines/stock/markets.json
    """
    return Moex.get_markets(engine=engine.value)


@router_moex_reference.get("/engines/{engine}/{market}/boards")
def moex_reference_engine_markets_boards(engine: ModelEngines,
                                         market: ModelMarkets,
                                         is_traded: Optional[bool] = None):
    """
    ### Метод для получения доступных режимов торгов рынка
        https://iss.moex.com/iss/reference/43
        - TQBR: Т+: Акции и ДР - безадрес.
        - TQCB: Т+: Облигации - безадрес.
        
    Args:
    - engine    : торговая система
    - market    : рынок

    Example:
    https://iss.moex.com/iss/engines/stock/markets/shares/boards.json
    """
    return Moex.get_boards(engine=engine.value,
                           market=market.value,
                           is_traded=is_traded)


@router_moex_reference.get("/markets")
def moex_reference_markets():
    """
    ### Метод для получения торговых систем
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="markets")


@router_moex_reference.get("/boards")
def moex_reference_boards():
    """
    ### Метод для получения режима торговли
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="boards")


@router_moex_reference.get("/boardgroups")
def moex_reference_boardgroups():
    """
    ### Метод для получения группы режимов торговли
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="boardgroups")


@router_moex_reference.get("/durations")
def moex_reference_durations():
    """
    ### Метод для получения расчетных интервалов свечей в формате HLOCV
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="durations")


@router_moex_reference.get("/securitytypes")
def moex_reference_securitytypes():
    """
    ### Метод для получения типов ценных бумаг
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="securitytypes")


@router_moex_reference.get("/securitygroups")
def moex_reference_securitygroups():
    """
    ### Метод для получения групп ценных бумаг
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="securitygroups")


@router_moex_reference.get("/securitycollections")
def moex_reference_securitycollections():
    """
    ### Метод для получения колекции ценных бумаг
    https://iss.moex.com/iss/reference/28

    Example:
    http://iss.moex.com/iss/index.json
    """
    return Moex.get_reference(block="securitycollections")
"""
Description: Working with Moscow exchange api(iis)
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 20.06.2023
Links:
    Описание методов:
    https://iss.moex.com/iss/reference/
    Руководство разработчика (v.1.4):
    https://fs.moex.com/files/6523
Comment:
"""

import json
import requests
from typing import Iterable, Optional
import pandas as pd


class MoexClientError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class URLMoex:
    """
    ### Класс для формирования url-запроса для IIS
    
    Args:
    - url_base          : базовый url, по умолчанию http://iss.moex.com/iss
    - url_head          : начало url, определяет блок данных, варианты определяются в HEAD_VALUES
    - url_engine        : добавить к url /engines/{engine}, торговую систему
        - stock     /Фондовый рынок и рынок депозитов
        - currency  /Валютный рынок
    - url_market        : добавить к url /market/{market}, рынок торговой системы
        - index     /Индексы фондового рынка
        - shares    /Рынок акций
        - bonds     /Рынок облигаций
    - url_board         : добавить к url /board/{board}, режим торгов
        - TQBR      /Т+: Акции и ДР - безадрес.
        - TQCB      /Т+: Облигации - безадрес.
    - url_boardgroups   : добавить к url /boardgroups/{boardgroups}, группу режимов торгов
    - url_security      : добавить к url /security/{security}, ценную бумагу
    - url_securitygroups: добавить к url /securitygroups/{securitygroups}, группу ценных бумаг
        - stock_index   /Индексы
        - stock_shares  /Акции
        - stock_bonds   /Облигации
    - url_collections   : добавить к url /collections/{collections}, коллекцию ценных бумаг
    - url_analytics     : добавить к url /analytics/{analytics}, аналитические показатели
    - url_assets        : добавить к url /assets/{assets}, опционные серии
    - url_yields        : добавить к url /yields/{yields}, доходность
    - url_tail          : окончание url, в свободной форме
    - url_columns       : добавить к url, /columns, расшифровку полей
    - iss_meta          : on|off метаинформацию – перечень, тип данных и размер полей
    - iss_data          : on|off включать или нет непосредственно рыночные данные
    - iss_json          : compact|extended – сокращенный или расширенный формат json
    - iss_only          : указывает, какие блоки нужно выгрузить
    - iss_columns       : указывает, какие колонки у блока нужно выгрузить
    - extension         : формат ответного собщения XML, CSV, JSON, HTML
    - option            : дополнительные парамметры, указываются в описании методов IIS
    """
    URL_BASE = "http://iss.moex.com/iss"

    EXTENSION = ["XML", "CSV", "JSON", "HTML", "xml", "csv", "json", "html"]

    HEAD_VALUES = ("statistics", "history", "archives", "rms", "events",
                   "events", "events")

    def __init__(self,
                 url_base: Optional[str] = None,
                 url_head: Optional[str] = None,
                 url_engine: Optional[str] = None,
                 url_market: Optional[str] = None,
                 url_board: Optional[str] = None,
                 url_boardgroups: Optional[str] = None,
                 url_security: Optional[str] = None,
                 url_securitygroups: Optional[str] = None,
                 url_collections: Optional[str] = None,
                 url_analytics: Optional[str] = None,
                 url_assets: Optional[str] = None,
                 url_yields: Optional[str] = None,
                 url_tail: Optional[str] = None,
                 url_columns: Optional[bool] = None,
                 iss_meta: Optional[bool] = None,
                 iss_data: Optional[bool] = None,
                 iss_json: Optional[str] = None,
                 iss_only: Optional[Iterable[str]] = None,
                 iss_columns: Optional[dict] = None,
                 extension: Optional[str] = None,
                 option: Optional[dict] = None):

        if url_base is None:
            self.url_base = self.URL_BASE
        else:
            self.url_base = url_base

        self.url_head = url_head
        self.url_engine = url_engine
        self.url_market = url_market
        self.url_board = url_board
        self.url_boardgroups = url_boardgroups
        self.url_security = url_security
        self.url_securitygroups = url_securitygroups
        self.url_collections = url_collections
        self.url_analytics = url_analytics
        self.url_assets = url_assets
        self.url_yields = url_yields
        self.url_tail = url_tail
        self.url_columns = url_columns
        self.iss_meta = iss_meta
        self.iss_data = iss_data
        self.iss_json = iss_json
        self.iss_only = iss_only
        self.iss_columns = iss_columns
        self.extension = extension
        self.option = option
        self.url = None

        self.check_attribute()

        self.url = self.get_url()

    def check_attribute(self) -> None:
        """
        ### Метод для проверки атрибутов класса
        """

        if self.extension is not None and self.extension not in self.EXTENSION:
            raise MoexClientError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"расширение self.extension = {self.extension} не поддерживается, " \
                f"список поддерживаемых - {self.EXTENSION}")

        if self.url_head is not None and self.url_head not in self.HEAD_VALUES:
            raise MoexClientError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"self.url_head = {self.url_head} не поддерживается, " \
                f"список поддерживаемых - {self.HEAD_VALUES}")

    def __str__(self) -> str:
        """
        ### Метод возвращает строковое представление объекта
        """

        return self.url

    def make_url(self) -> str:
        """
        ### Метод формирует основную часть url
        """

        url_parts = []
        if self.url_head:
            url_parts.append(f"/{self.url_head}")
        if self.url_engine:
            url_parts.append(f"/engines/{self.url_engine}")
        if self.url_market:
            url_parts.append(f"/markets/{self.url_market}")
        if self.url_board:
            url_parts.append(f"/boards/{self.url_board}")
        if self.url_boardgroups:
            url_parts.append(f"/boardgroups/{self.url_boardgroups}")
        if self.url_security:
            url_parts.append(f"/securities/{self.url_security}")
        if self.url_securitygroups:
            url_parts.append(f"/securitygroups/{self.url_securitygroups}")
        if self.url_collections:
            url_parts.append(f"/collections/{self.url_collections}")
        if self.url_analytics:
            url_parts.append(f"/analytics/{self.url_analytics}")
        if self.url_assets:
            url_parts.append(f"/assets/{self.url_assets}")
        if self.url_yields:
            url_parts.append(f"/yields/{self.url_yields}")
        if self.url_tail:
            url_parts.append(f"/{self.url_tail}")
        if self.url_columns:
            url_parts.append(f"/columns")
        if self.extension:
            url_parts.append(f".{self.extension}")

        return "".join(url_parts)

    def make_option(self) -> str:
        """
        ### Метод формирует дополнительные парамметры
        """

        option_parts = []

        if self.option is not None and self.option:
            for key, value in self.option.items():
                option_parts.append(f"{key}={value}")

        return "&".join(option_parts)

    def make_iss(self) -> str:
        """
        ### Метод формирует системные парамметры
        """

        iss_parts = []
        # iss.meta
        if self.iss_meta is not None:
            if self.iss_meta:
                iss_parts.append("iss.meta=on")
            else:
                iss_parts.append("iss.meta=off")

        # iss.data
        if self.iss_data is not None:
            if self.iss_data:
                iss_parts.append("iss.data=on")
            else:
                iss_parts.append("iss.data=off")

        # iss_json
        if self.iss_json is not None:
            if self.iss_json:
                iss_parts.append("iss.json=extended")
            else:
                iss_parts.append("iss.json=compact")

        # iss.only
        if self.iss_only is not None and self.iss_only:
            iss_parts.append(f"iss.only={self.iss_only}")

        # iss.columns
        if self.iss_columns is not None and self.iss_columns:
            for key, value in self.iss_columns.items():
                iss_parts.append(f"{key}.columns={value}")

        return "&".join(iss_parts)

    def get_url(self) -> str:
        """
        ### Метод собирает все части в итоговый url
        """

        url_final = []
        option = self.make_option()
        iis = self.make_iss()

        url_final.append(self.url_base)

        url_final.append(self.make_url())

        if option or iis:
            if "?" in self.url_base:
                url_final.append("&")
            else:
                url_final.append("?")

        url_final.append(option)

        if option and iis:
            url_final.append("&")

        url_final.append(iis)

        return "".join(url_final)


class DataMoex:
    """
    ### Класс для получения данных от IIS
    
    Args:
    - url   : url для получения данных
    """

    def __init__(self, url: str):
        self.url = url

    def query(self, query_url: str) -> bytes:
        """
        ### Метод HTTP-запроса
        
        Args:
        - query_url   : url для получения данных
        """

        r = requests.get(url=query_url)
        r.encoding = 'utf-8'

        if r.status_code != 200:
            raise MoexClientError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"http ответ- {r.status_code}, нужен 200")

        return r.content

    def get_data(self, blockname_parts: list = ["history"]) -> json:
        """
        ### Метод для получения данных, единая точка входа.
        Данные могут отправляться не целиком, а по 100 записей в блоке
                
        Args:
        - blockname_parts   : имя блока, по которому предполагается получения данных частями,
                              чаще всего такой блок называется history.
        """

        convert_data = {}

        if ".json" not in self.url.lower():
            raise MoexClientError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"в url отстутсвует расширение .json, " \
                f"метод работает только с json данными")

        j = self.convert_to_json(self.query(query_url=self.url))

        for blockname in j.keys():
            count_row = len(j[blockname]['data'])
            if blockname in blockname_parts and count_row >= 100:
                convert_data[blockname] = self.get_data_parts(
                    blockname=blockname)[blockname]
            else:
                convert_data[blockname] = self.convert_to_array(
                    j=j, blockname=blockname)

        return convert_data

    def get_data_pandas(self, blockname: str) -> pd.core.frame.DataFrame:
        """
        ### Метод для получения данных, в виде Pandas.DataFrame
                
        Args:
        - blockname   : имя блока, по которому предполагается получения данных
        """
        return self.convert_to_pandas(self.get_data()[blockname])

    def get_data_parts(self,
                       blockname: str,
                       start: int = 0,
                       portion: int = 100):
        """
        ### Метод для получения всех данный разбитых на несколько частей
        для этого нужно последовательно формировать url вида:
        http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/MOEX
        http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/MOEX?start=100
        http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/MOEX?start=200

        Args:
        - blockname : имя блока, по которому предполагается получения данных
        - start     : cтартовая позиция 
        - portion   : размер разовой порции(макимально 100,возможны значения 50, 20, 10, 5, 1)
        """

        while_start = start
        data_all = []
        while True:
            data = self.get_data_part(blockname=blockname, start=while_start)
            if len(data[blockname]) == 0:
                break

            data_all.extend(data[blockname])
            while_start = while_start + portion

        return {blockname: data_all}

    def get_data_part(self, blockname: str, start: int, limit: int = 100):
        """
        ### Метод для получения части данных, формирует url вида:
        http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/MOEX?start=200

        Args:
        - blockname : имя блока, по которому предполагается получения данных
        - start     : cтартовая позиция 
        - portion   : размер разовой порции(макимально 100,возможны значения 50, 20, 10, 5, 1)
        """

        if limit > 100:
            raise MoexClientError(
                f"Ошибка в {self.__class__.__name__}: " \
                f"get_data_part.limit не может быть больше 100. " \
                f"Ограничение системы ИИС")

        query_url = URLMoex(
            url_base=self.url,
            iss_only=blockname,
            option={
                "start": start,
                "limit": limit
            },
        )
        j = self.convert_to_json(self.query(query_url))
        #print(query_url)
        return {blockname: self.convert_to_array(j=j, blockname=blockname)}

    def convert_to_pandas(self, j: dict) -> pd.core.frame.DataFrame:
        """
        ### Метод для конвертирования json в Pandas.DataFrame:

        Args:
        - j : json, должен быть вида {column1:value, column2:value}
        """

        df = pd.json_normalize(j)
        return df

    def convert_to_json(self, content: str) -> json:
        """
        ### Метод для конвертирования данных в json

        Args:
        - content : входные данные
        """

        j = json.loads(content)
        return j

    def convert_to_array(self, j: dict, blockname: str):
        """
        ### Метод для формирования двумерного массива(в том числе для конвертирования в pandas)

        Args:
        - j         : входной json
        - blockname : имя блока, по которому предполагается получения данных
        """

        return [{
            str.lower(k): r[i]
            for i, k in enumerate(j[blockname]['columns'])
        } for r in j[blockname]['data']]
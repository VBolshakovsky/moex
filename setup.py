from setuptools import find_packages, setup

setup(
    name='moex_gate',
    packages=find_packages(),
    version='0.1.0',
    description='Project for working with the Moscow Exchange',
    author='V.Bolshakov',
)
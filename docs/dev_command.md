# Requirements

python -m venv venv
venv\Scripts\Activate.ps1
. bin/activate

python -m pip install --upgrade pip

pip install -r .\moex_gate\requirements\base.txt

pip install  -e .\moex_gate

pip freeze > .\moex_gate\requirements\prod.txt

pip uninstall -r -y .\moex_gate\requirements\base.txt
pip uninstall -y moex_gate

# Docker

docker build --pull --rm -f "moex_gate\dockerfile" -t ubsgate:latest "moex_gate"
docker run --dns=8.8.8.8 -t -i moex_gate /bin/sh
docker compose -f "moex_gate\docker-compose.yml" up -d --build
docker compose -f "moex_gate\docker-compose.yml" down

# FASTAPI

cd moex_gate\src

uvicorn main:app_api --reload
uvicorn main:app_api --reload --port 8000 --host 0.0.0.0 --workers 4

<http://127.0.0.1:8000>
<http://127.0.0.1:8000/docs>
<http://127.0.0.1:8000/redoc>

# Celery

celery -A src.tasks.tasks:app_celery worker -l INFO -n moex_beat --pool=solo
celery -A src.tasks.tasks:app_celery beat -l INFO -s app\log\celery\schedule\schedule.db
celery -A src.tasks.tasks:app_celery flower -l INFO

celery -A src.tasks.tasks:app_celery worker -l ERROR -n moex_beat --pool=solo
celery -A src.tasks.tasks:app_celery beat -l ERROR -s app\log\celery\schedule\schedule.db
celery -A src.tasks.tasks:app_celery flower -l ERROR
<http://localhost:5555/>
